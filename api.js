const express = require('express')
const fs = require('fs');
const path = require('path')
const router = express.Router();
const appDir = path.dirname(require.main.filename);
const filesDirPath = path.join(appDir, 'files')
const { readFile, readdir, stat, unlink, writeFile } = require("fs/promises");
const supportedExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js']



router.get('/', async function(req, res) {

    const list = await readdir(filesDirPath)
    res.status(200).json({
        "message": "Success",
        "files": list
    })

})

router.get('/:filename', async function(req, res) {
    try {
        const filename = req.params.filename
        const filePath = path.join(filesDirPath, filename)
        const content = await readFile(filePath, { encoding: "utf8" })
        const fileInfo = await stat(filePath)
        const bdTime = fileInfo.birthtime
        res.status(200).json({
            "message": "Success",
            "filename": filename,
            "content": content,
            "extension": path.extname(filename).replace('.', ''),
            "uploadedDate": bdTime
        })
    } catch (error) {
        res.status(400).json({
            "message": error.message
        })
    }

})

router.post('/', async function(req, res) {
    let { filename, content } = req.body
    const filePath = path.join(filesDirPath, filename)
    const fileExt = path.extname(filename);
    let isSuportedExt = supportedExt.some(ext => `.${ext}` === fileExt)

    if (filename && content && isSuportedExt) {
        writeFile(filePath, content).then(() => {
            res.status(200).json({ "message": "File created successfully" })
        }).catch(err => {
            res.status(400).json({
                "message": `${err.message}`
            })
        })

        /*         fs.writeFile(`${filesDirPath}/${filename}`, content, (err) => {
                    if (err) {
                        throw err
                    }
                    res.status(200).json({ "message": "File created successfully" })
                }); */
    } else {
        console.log("issuported ======================", isSuportedExt)
        let message = ''
        message = filename ? 'filename' : message
        message = content ? 'content' : message
        message = isSuportedExt ? message : 'file ext'
        return res.status(400).json({
            "ext": `${fileExt}`,
            "message": `Please specify ${message} parameter`
        })
    }
});

router.put('/:filename', async function(req, res) {
    let { filename, content } = req.body
    const fileExt = path.extname(filename);
    let isSuportedExt = supportedExt.some(ext => `.${ext}` === fileExt)

    if (filename && content && isSuportedExt) {
        fs.writeFile(`${filesDirPath}/${filename}`, content, (err) => {
            if (err) {
                throw err
            }
            res.status(200).json({ "message": "File created successfully" })
        });
    } else {
        let message = ''
        message = filename ? 'filename' : message
        message = content ? 'content' : message
        message = isSuportedExt ? 'file ext' : message

        return res.status(400).json({
            "ext": `${fileExt}`,
            "message": `Please specify ${message} parameter`
        })
    }
});


router.delete('/:filename', async function(req, res, next) {
    const filename = req.params.filename
    const filePath = path.join(filesDirPath, filename)
    unlink(filePath).then(res => {
        if (res === undefined) {
            res.status(200).json({ "message": "deleted succsessfully" })
        }
    }).catch(err => {
        res.status(400).json({ "message": "no such file" })
    })
})


module.exports = router