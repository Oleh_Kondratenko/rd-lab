const express = require('express')
const PORT = 8080;
const app = express()
const api = require('./api.js');
const logger = require('morgan')

app.use(logger('dev'));
app.use(express.json())

app.use('/api/files', api)
app.use((err, req, res, next) => {
    if (err) {
        res.status(400).json({
            "message": `${err.message}`
        })
    }
    next()
});


const server = app.listen(PORT, function() {
    console.log(`app listening on port ${PORT}`)
})